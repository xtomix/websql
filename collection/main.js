		var DB = null;

		var Model = Backbone.Model.extend({
			defaults : {
				name 	: ""
			}
		});

		var Collection = Backbone.Collection.extend({
			initialize : function(options) {
				this.options = options || {};

				this.openDB();
				this.createMasterTable();

				this.readItems();
			},
			model : Model,
			getTableAttr : function() {
				var model 			= new Model(),
						tableName 	= this.options.tableName,
						arrColumns 	= []
				;

				var arrColumns = Object.keys(model.attributes);

				return {
					"tableName"		: tableName,
					"arrColumns"	: arrColumns
				};
			},
			openDB : function() {
				var dbName 	= "newcccgcfb",
						dbSize 	= 5 * 1024 * 1024
				;

				DB = openDatabase(dbName, "1.0", dbName, dbSize, function(data) {
						console.log("new database created: " + dbName);
						console.log(data);
					}
				);
			},
			createMasterTable : function() {
				var tableName	= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns
				;
				var that = this;

				var sqlMC = "CREATE TABLE IF NOT EXISTS `master` (id INTEGER PRIMARY KEY ASC, table_name TEXT, columns BLOB);";
				var sqlMS = "SELECT * from `master`;";
				var sqlMI = "INSERT INTO `master` (table_name, columns) VALUES (?, ?);";

				this.runDB(
					sqlMC,
					[],
					function(tx, resultC) {
						that.runDB(
							sqlMS,
							[],
							function(tx, resultS) {
								console.log("master table:");
								console.log(resultS.rows.length);
								var exists = false, arrAttr;
								for(var i = 0, ln = resultS.rows.length; i < ln; i++) {
									console.log(resultS.rows.item(i));
									if(resultS.rows.item(i).table_name === tableName) {
										exists = true;
										arrAttr = resultS.rows.item(i).columns.split(",");
									}
								}

								if(!exists) {
									console.log("a " + tableName + " table doesnt exist. i'll save it into master and create it too");
									//that.createTable();
									that.runDB(
										sqlMI,
										[tableName, arrColumns],
										function() {
											that.createTable();
										}
									);
								} else {
									console.log("the table already exists");
									if($(arrColumns).not(arrAttr).length == 0 && $(arrAttr).not(arrColumns).length == 0) {
										console.log("same columns");
									} else {
										console.log("not the same columns");
										console.log("original");
										console.log(arrAttr);
										console.log("new");
										console.log(arrColumns);
									}
								}
							}
						)
					}
				);
			},
			createTable : function() {

				var tableName	= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns
				;

				var sql = "CREATE TABLE IF NOT EXISTS "	+ tableName + " (id INTEGER PRIMARY KEY ASC, ";
				for(var i = 0, ln = arrColumns.length; i < ln; i++) {
					if(i < ln -1) {
						sql +=  arrColumns[i] + " TEXT, ";
					} else {
						sql +=  arrColumns[i] + " TEXT) ";
					}
				}
				console.log(sql);

				this.runDB(
					sql,
					[]
				);
			},
			dropTable : function(){
				var tableName	= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns,
						sql			=  'DROP ' + tableName
				;

				this.runDB(
					sql,
					[]
				);
			},
			createItem : function(data) {
				var tableName		= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns
				;

				var that = this;

				var sql =  'INSERT INTO ' + tableName + '(';
				for(var i = 0, ln = arrColumns.length; i < ln; i++) {
					if(i < ln -1) {
						sql +=  arrColumns[i] + ", ";
					} else {
						sql +=  arrColumns[i] + ") VALUES (";
					}
				}
				for(var i = 0, ln = arrColumns.length; i < ln; i++) {
					if(i < ln -1) {
						sql += "?, ";
					} else {
						sql +=  "?)";
					}
				}

				var dataToBeInsterted = [];
				for(var i = 0, ln = data.length; i < ln; i++) {
					dataToBeInsterted.push(data[i]);
				}

				this.runDB(
					sql,
					dataToBeInsterted,
					function(tx, result) {
						var modelData = {};
						modelData.id = result.insertId;
						for(var i = 0, ln = arrColumns.length; i < ln; i++) {
							modelData[arrColumns[i]] = dataToBeInsterted[i];
						}
						that.add(new Model(modelData));
						console.log("create success")
					},
					function() {
						console.log("create failure")
					}
				);
			},
			readItems : function() {
				var tableName		= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns,
						sql 				= "SELECT * FROM " + '`' + tableName + '`'
				;

				var that = this;

				this.runDB(
					sql,
					[],
					function(tx, result) {
						var modelData = {};

						for(var i = 0, ln = result.rows.length; i < ln; i++) {
							modelData.id = result.rows.item(i).id;
							for(var j = 0, lnJ = arrColumns.length; j < lnJ; j++) {
								modelData[arrColumns[j]] = result.rows.item(i)[arrColumns[j]];
							}
							console.log(modelData);
							that.add(new Model(modelData));
						}

						console.log("read success");
					},
					function() {
						console.log("read failure");
					}
				);
			},
			updateItem : function(id, arrColumnsToBeUpdated, arrValues, model) {
				var tableName	= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns
				;

				var that = this;

				var sql = "UPDATE " + tableName + " SET "
				for(var i = 0, ln = arrColumnsToBeUpdated.length; i < ln; i++) {
					if(i < ln - 1) {
						sql += arrColumnsToBeUpdated[i] + " = ?, ";
					} else {
						sql += arrColumnsToBeUpdated[i] + " = ? ";
					}

				}
				sql += "WHERE id = ?";

				arrValues.push(id);

				this.runDB(
					sql,
					arrValues,
					function() {
						var modelData = {};
						for(var i = 0, ln = arrColumnsToBeUpdated.length; i < ln; i++) {
							modelData[arrColumnsToBeUpdated[i]] = arrValues[i];
						}
						model.set(modelData);
						console.log("update success");
					}
				);

			},
			deleteItem : function(id, model) {
				var tableName	= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns,
						sql 		= "DELETE FROM " + tableName + " WHERE id = ?"
				;

				var that = this;

				this.runDB(
					sql,
					[id],
					function() {
						that.remove(model);
						console.log("delete success");
					}
				);

			},
			deleteAll : function() {
				var tableName	= this.getTableAttr().tableName,
						arrColumns	= this.getTableAttr().arrColumns,
						sql 		= "DELETE FROM " + tableName
				;

				this.runDB(
					sql,
					[]
				);
			},
			runDB : function(sql, arr, success, failure) {
				DB.transaction(function(tx) {
					tx.executeSql(
						sql,
						arr,
						success,
						failure
					);
				});
			}
		});

		var ItemView = Backbone.View.extend({
			tagName : 'li',
			initialize : function() {},
			events : {
				'click [data-button="set"]' 		: 'setItem',
				'click [data-button="delete"]' 	: 'deleteItem'
			},
			template : _.template($('[data-view="item"]').html()),
			render : function() {
				this.$el.html(this.template(this.model.toJSON()));
				return this;
			},
			setItem : function() {
				var name = prompt("", this.model.get('name'));
				this.collection.updateItem(this.model.get('id'), ["name"], [name], this.model);
			},
			deleteItem : function() {
				this.collection.deleteItem(this.model.get('id'), this.model);
			}
		});

		var ListView = Backbone.View.extend({
			tagName : 'ul',
			initialize : function() {
				this.collection.on('add', this.render, this);
				this.collection.on('remove', this.render, this);
				this.collection.on('change', this.render, this);
			},
			events : {},
			template : "",
			render : function() {
				this.$el.html('');
				this.collection.each(function(model) {
					var itemView = new ItemView({model : model, collection : this.collection});
					this.$el.append(itemView.render().el);
				}, this);
				return this;
			}
		});

		var AddView = Backbone.View.extend({
			el : '[data-container="main"]',
			initialize : function() {
				this.render();
				this.collection.reset(); // a must because we dont need the init model with tablename anymore
				var listView = new ListView({collection : this.collection});
				this.$el.append(listView.render().el);
			},
			events : {
				'click [data-button="add"]' : 'addItem'
			},
			template : _.template($('[data-view="add"]').html()),
			render : function() {
				this.$el.html(this.template());
				return this;
			},
			addItem : function() {
				var name = this.$el.find('[data-input="add"]').val();
				this.collection.createItem([name]);
				this.$el.find('[data-input="add"]').val('');
			}
		});

		var addView = new AddView({
			collection : new Collection({"tableName" 	: "affxsg"})
		});
