// >> NAMESPACE
var myApp 		= {};
myApp.E 	= {} 	|| "";  // events
myApp.C 	= {} 	|| "";  // controllers

myApp.B 	= {}	|| "";  // backbone things
myApp.S 	= {} 	|| "";  // server side controllers
myApp.DB 	= null;         //  database
myApp.SQL 	= {} 	|| "";  // sql sentences

// << NAMESPACE



// >> BACKBONE MODEL
var TodoItem = Backbone.Model.extend({
	"initialize" : function() {
		this.on('add', function() {
			console.log("new model item has been created");
		});
		this.on('change', function() {
			console.log("the model has been changed");
		});
		this.on('remove', function() {
			console.log("a model has been removed");
		});
	}
});
// << BACKBONE MODEL

// >> BACKBONE COLLECTION
var TodoItemCollection = Backbone.Collection.extend({
	"model" : TodoItem
	,	"initialize" : function() {
		console.log("new collection has been created");
	}
	,	"enumerate" : function() {
		console.log("collection contains:");
		for(var i = 0, ln = this.models.length; i < ln; i++ ) {
			console.log(this.models[i].cid + " " + JSON.stringify(this.models[i].attributes));
		}
	}
});

var todoItemCollection = new TodoItemCollection();
// << BACKBONE COLLECTION



// >> EVENTS
$(document).ready(function() {
	myApp.C.init();
});

myApp.E = {
	"attachEvents" : function() {
		// >> query for experimental queries
		$('[data-button="query"]').off().on('click', function() {
			myApp.C.queryDB();
		});
		// << query for experimental queries

		$('[data-button="create-item"]').off().on('click', function() {
			myApp.C.createItem();
		});

		$('[data-button="update-item"]').off().on('click', function() {
			var id = $(this).data('id');
			myApp.C.updateItem(id);
		});

		$('[data-button="delete-item"]').off().on('click', function() {
			var id = $(this).data('id');
			myApp.C.deleteItem(id);
		});

		$('[data-button="delete-all-items"]').off().on('click', function() {
			var id = $(this).data('id');
			myApp.C.deleteAllItems();
		});
	}
};
// << EVENTS



// >> SQL sentences
myApp.SQL = {
	"query" : function() {
		var sql = "SELECT * from todo;";
		return sql;
	}
	,	"createTable" : function() {
		var sql = "CREATE TABLE IF NOT EXISTS todo(id INTEGER PRIMARY KEY ASC, todo TEXT, added_on DATETIME)";
		return sql;
	}
	,	"createItem" : function() {
		var sql = "INSERT INTO todo(todo, added_on) VALUES (?,?)";
		return sql;
	}
	,	"readItems" : function() {
		var sql = "SELECT * FROM todo";
		return sql;
	}
	,	"readItem" :function() {
		var sql = "SELECT `todo`, `added_on` FROM `todo` WHERE id = ?";
		return sql;
	}
	,	"updateItem" : function() {
		var sql = "UPDATE todo SET todo = ? WHERE id = ?";
		return sql;
	}
	,	"deleteItem" : function() {
		var sql = "DELETE FROM todo WHERE id = ?";
		return sql;
	}
	,	"deleteAllItems" : function() {
		var sql = "DELETE FROM todo";
		return sql;
	}
};
// << SQL sentences



// >> CLIENT SIDE CONTROLLERS
myApp.C = {
	"init" : function() {
		myApp.S.openDB();
		myApp.S.createTable();
		myApp.S.readItems(myApp.C.readItems);
	}
	,	"queryDB" : function() {
		myApp.S.queryDB();
	}
	,	"createItem" : function() {
		var todoItem = $('[data-id="todo-item"]');
		myApp.S.createItem(todoItem.val());
		todoItem.val("");
	}
	,	"readItems" : function(tx, result) {
		var rowOutput = "";
		var todoItems = $('[data-container="todo-items"]');
		var item = [];

		for (var i=0; i < result.rows.length; i++) {

			item[i] = {
				"idDb" : result.rows.item(i).ID
				,	"todo" : result.rows.item(i).todo
				,	"date" : result.rows.item(i).added_on
			};

			rowOutput += '<li>' + result.rows.item(i).todo
				+ '<button type="button" data-button="update-item" data-id="'	+ result.rows.item(i).ID + '">Set</button>'
				+ '<button type="button" data-button="delete-item" data-id="'	+ result.rows.item(i).ID + '">Delete</button>'
				+ '</li>'
			;
		}

		if(todoItemCollection.models.length === 0) {
			todoItemCollection.add(item);
		}

		console.log("---------------------------------------");
		todoItemCollection.enumerate();
		todoItems.html(rowOutput);
		myApp.E.attachEvents();
	}
	,	"updateItem" : function(id) {
		myApp.S.readItem(
			id
			,	function(tx, result) {
				var todo = prompt("Please enter your name", result.rows.item(0).todo);
				myApp.S.updateItem(id, todo);
			}
		);
	}
	,	"deleteItem": function(id) {
		myApp.S.readItem(
			id
			,	function(tx, result) {
				var delTodo = confirm("biztos törlöd ezt: " + result.rows.item(0).todo + "?");
				if(delTodo) {
					myApp.S.deleteItem(id);
				}
			}
		);
	}
	,	"deleteAllItems" : function() {
		if(confirm("tuti?")) {
			myApp.S.deleteAllItems();
		}
	}
};
// << CLIENT SIDE CONTROLLERS



// >> SERVER SIDE CONTROLLERS - MODELS
myApp.S = {
	"openDB" : function() {
		var dbSize = 5 * 1024 * 1024; // 5MB
		myApp.DB = openDatabase("Todo", "1.0", "Todo manager", dbSize, function(data) {console.log(data);});
	}
	,	"queryDB" : function() {
		myApp.S.run(
			myApp.SQL.query()
			,	[]
			,	function(tx, data) {
				console.log(tx);
				console.log(data);
			}
			,	myApp.S.onError
		);
	}
	,	"createTable":function() {
		myApp.S.run(
			myApp.SQL.createTable()
			,	[]
		);
	}
	,	"createItem" : function(todoItem) {
		var addDate = new Date();
		addDate = addDate.toUTCString();
		myApp.S.run(
			myApp.SQL.createItem()
			,	[todoItem, addDate]
			,	function(tx, result) {
				var id = result.insertId;
				myApp.S.readItem(id, function(tx, result) {
					var item = new TodoItem(
						{
							"idDb" : id
							,	"todo" : result.rows.item(0).todo
							,	"date" : result.rows.item(0).added_on
						}
					);
					todoItemCollection.add(item);
					myApp.S.readItems(myApp.C.readItems);
				});
			}
			,	myApp.S.onError
		);
	}
	,	"readItems" : function(cb) {
		myApp.S.run(
			myApp.SQL.readItems()
			,	[]
			,	cb
			,	myApp.S.onError
		);
	}
	,	"readItem" : function(id, cb) {
		myApp.S.run(
			myApp.SQL.readItem()
			,	[id]
			,	cb
			,	myApp.S.onError
		);
	}
	,	"updateItem" : function(id, todo) {
		myApp.S.run(
			myApp.SQL.updateItem()
			,	[todo, id]
			,	function(tx, result) {
				var item = todoItemCollection.where({"idDb" : id})[0];
				item.set({"todo" : todo});
				myApp.S.readItems(myApp.C.readItems);
			}
			,	myApp.S.onError
		);
	}
	,	"deleteItem" :function(id) {
		myApp.S.run(
			myApp.SQL.deleteItem()
			,	[id]
			,	function(tx, result) {
				var cid = todoItemCollection.where({"idDb" : id})[0].cid;
				todoItemCollection.remove(cid);
				myApp.S.readItems(myApp.C.readItems);
			}
			,	myApp.S.onError
		);
	}

	,	"deleteAllItems": function(id) {
		myApp.S.run(
			myApp.SQL.deleteAllItems()
			,	[]
			,	function() {
				todoItemCollection.reset();
				myApp.S.readItems(myApp.C.readItems);
			}
			,	myApp.S.onError
		);
	}
	,	"onError" : function(tx, e) {
		alert("There has been an error: " + e.message);
	}
	,	"run" : function(sql, arr, success, failure) {
		myApp.DB.transaction(function(tx) {
			tx.executeSql(
				sql
				,	arr
				,	success
				,	failure
			);
		});
	}
};

// << SERVER SIDE CONTROLLERS - MODELS



