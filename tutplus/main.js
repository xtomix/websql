
	(function() {
		window.App = {
			Model		: {},
			Collection	: {},
			View 		: {},
			Helper		: {}
		};


		App.Helper.template = function(id) {
			return _.template($('#' + id).html());
		};


		App.Model.Task = Backbone.Model.extend({
			initialize : function() {
			},

			validate : function(attr) {
				if(!$.trim(attr.title)) {
					return 'you need a title'
				}
			}
		});


		App.Collection.Tasks = Backbone.Collection.extend({
			model : App.Model.Task
		});


		App.View.Task = Backbone.View.extend({
			tagName : 'li',

			initialize : function() {
				this.model.on('change', this.render, this);
				this.model.on('destroy', this.remove, this);
				this.model.on('invalid', function(model, error) {
					console.log(error);
				});
			},

			events : {
				'click .edit' : 'editTask',
				'click .delete' : 'deleteTask'
			},

			template : App.Helper.template('taskTemplate'),

			render : function() {
				var template = this.template(this.model.toJSON());
				this.$el.html(template);
				return this;
			},

			editTask : function() {
				var newTaskTitle = prompt('What would you like to change the text to?', this.model.get('title'));
				this.model.set({title : newTaskTitle}, {validate : true});
			},

			deleteTask : function() {
				this.model.destroy();
			},

			remove : function() {
				this.$el.remove();
			}
		});


		App.View.Tasks = Backbone.View.extend({
			tagName : 'ul',

			initialize : function() {
				this.collection.on('add', this.addOne, this);
			},

			render : function() {
				this.collection.each(this.addOne, this);
				return this;
			},

			addOne : function(task) {
				var taskView = new App.View.Task({model : task});
				this.$el.append(taskView.render().el);
			}
		});


		App.View.AddTask = Backbone.View.extend({
			el : '#addTask',

			initialize : function() {

			},

			events : {
				'click [data-button="add-task"]' : 'addTask'
			},

			addTask : function() {
				var newTaskTitle = this.$el.find('[data-input="add-task"]').val();

				var task = new App.Model.Task({title : newTaskTitle});
				this.collection.add(task);
				this.$el.find('[data-input="add-task"]').val('');
			}
		});

		//////////////////////////////////////
		var tasksCollection = new App.Collection.Tasks([
			{
				title : "werk",
				priority : 3
			},
			{
				title : "drg",
				priority : 36
			},
			{
				title : "werhfhk",
				priority : 43
			}
		]);

		var addTaskView = new App.View.AddTask({collection : tasksCollection});

		var tasksView = new App.View.Tasks({collection : tasksCollection});

		$('.tasks').html(tasksView.render().el);
	})();
