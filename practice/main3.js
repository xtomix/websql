	var Model = Backbone.Model.extend();

	var ItemView = Backbone.View.extend({
		tagName : 'li',
		initialize : function() {

		},
		events : {
			'click [data-button="set"]' 	: 'setItem',
			'click [data-button="delete"]'	: 'deleteItem'
		},
		template : _.template($('#item').html()),
		render : function() {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},
		setItem : function() {
			var name = prompt();
			this.model.set({name : name});
			this.render();
		},
		deleteItem : function() {
			this.remove();
		}
	});

	var ListView = Backbone.View.extend({
		el : '[data-container="main"]',
		initialize : function() {
			this.render();
		},
		events : {
			'click [data-button="add"]' : 'addItem'
		},
		template : _.template($('#main').html()),
		render : function() {
			this.$el.html(this.template());
			return this;
		},
		addItem : function() {
			var name = this.$el.find('input').val();
			var model = new Model({name : name});
			var itemView = new ItemView({model : model});
			this.$el.append(itemView.render().el);
			this.$el.find('input').val('');
		}
	});

	var listView = new ListView();

	$(document.body).append(listView.render().el);