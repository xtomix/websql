	// >> NAMESPACE
		var myApp 		= {};
			myApp.E 	= {} 	|| "";  // events

			myApp.B 	= {}	|| "";  // backbone things

			myApp.DB 	= null;         //  database
			myApp.SQL 	= {} 	|| "";  // sql sentences

	// << NAMESPACE

	// >> EVENTS
	myApp.E = {
		"attachEvents" : function() {
			// >> query for experimental queries
			$('[data-button="query"]').off().on('click', function() {
				todoItemCollection.queryDBC();
			});
			// << query for experimental queries

			$('[data-button="create-item"]').off().on('click', function() {
				todoItemCollection.createItemC();
			});

			$('[data-button="update-item"]').off().on('click', function() {
				var id = $(this).data('id');
				todoItemCollection.updateItemC(id);
			});

			$('[data-button="delete-item"]').off().on('click', function() {
				var id = $(this).data('id');
				todoItemCollection.deleteItemC(id);
			});

			$('[data-button="delete-all-items"]').off().on('click', function() {
				var id = $(this).data('id');
				todoItemCollection.deleteAllItemsC();
			});
		}
	};
	// << EVENTS



	// >> SQL sentences
	myApp.SQL = {
		"query" : function() {
			var sql = "SELECT * from todo;";
			return sql;
		}
		,	"createTable" : function() {
			var sql = "CREATE TABLE IF NOT EXISTS todo(id INTEGER PRIMARY KEY ASC, todo TEXT, added_on DATETIME)";
			return sql;
		}
		,	"createItem" : function() {
			var sql = "INSERT INTO todo(todo, added_on) VALUES (?,?)";
			return sql;
		}
		,	"readItems" : function() {
			var sql = "SELECT * FROM todo";
			return sql;
		}
		,	"readItem" :function() {
			var sql = "SELECT `todo`, `added_on` FROM `todo` WHERE id = ?";
			return sql;
		}
		,	"updateItem" : function() {
			var sql = "UPDATE todo SET todo = ? WHERE id = ?";
			return sql;
		}
		,	"deleteItem" : function() {
			var sql = "DELETE FROM todo WHERE id = ?";
			return sql;
		}
		,	"deleteAllItems" : function() {
			var sql = "DELETE FROM todo";
			return sql;
		}
	};
	// << SQL sentences



	// >> BACKBONE MODEL
		var TodoItem = Backbone.Model.extend({
			"initialize" : function() {
				this.on('add', function() {
					console.log("new model item has been created");
				});
				this.on('change', function() {
					console.log("the model has been changed");
				});
				this.on('remove', function() {
					console.log("a model has been removed");
				});
			}
		});
	// << BACKBONE MODEL

	// >> BACKBONE COLLECTION
		var ParentCollection = Backbone.Collection.extend({
			"model" : TodoItem
		,	"initialize" : function() {
				console.log("new collection has been created");
				this.initC();
			}
		,	"enumerate" : function() {
				console.log("collection contains:");
				for(var i = 0, ln = this.models.length; i < ln; i++ ) {
					console.log(this.models[i].cid + " " + JSON.stringify(this.models[i].attributes));
				}
			}
		,	"initC" : function() {
				this.openDBSE();
				this.createTableSE();
				this.readItemsSE(this.readItemsC);
			}
		,	"queryDBC" : function() {
				this.queryDBSE();
			}
		,	"createItemC" : function() {
				var todoItem = $('[data-id="todo-item"]');
				this.createItemSE(todoItem.val());
				todoItem.val("");
			}
		,	"readItemsC" : function(tx, result) {
				var rowOutput = "";
				var todoItems = $('[data-container="todo-items"]');
				var item = [];

				for (var i=0; i < result.rows.length; i++) {

					item[i] = {
						"idDb" : result.rows.item(i).ID
					,	"todo" : result.rows.item(i).todo
					,	"date" : result.rows.item(i).added_on
					};

					rowOutput += '<li>' + result.rows.item(i).todo
						+ '<button type="button" data-button="update-item" data-id="'	+ result.rows.item(i).ID + '">Set</button>'
						+ '<button type="button" data-button="delete-item" data-id="'	+ result.rows.item(i).ID + '">Delete</button>'
						+ '</li>'
					;
				}

				if(todoItemCollection.models.length === 0) {
					todoItemCollection.add(item);
				}

				console.log("---------------------------------------");
				todoItemCollection.enumerate();
				todoItems.html(rowOutput);
				myApp.E.attachEvents();
			}
		,	"updateItemC" : function(id) {
				var that = this;
				this.readItemSE(
					id
				,	function(tx, result) {
						var todo = prompt("Please enter your name", result.rows.item(0).todo);
						that.updateItemSE(id, todo);
					}
				);
			}
		,	"deleteItemC": function(id) {
				var that = this;
				this.readItemSE(
					id
				,	function(tx, result) {
						var delTodo = confirm("biztos törlöd ezt: " + result.rows.item(0).todo + "?");
						if(delTodo) {
							that.deleteItemSE(id);
						}
					}
				);
			}
		,	"deleteAllItemsC" : function() {
				if(confirm("tuti?")) {
					this.deleteAllItemsSE();
				}
			}
		,	"openDBSE" : function() {
				var dbSize = 5 * 1024 * 1024; // 5MB
				myApp.DB = openDatabase("Todo", "1.0", "Todo manager", dbSize, function(data) {console.log(data);});
			}
		,	"queryDBSE" : function() {
				var that = this;
				this.runSE(
					myApp.SQL.query()
				,	[]
				,	function(tx, data) {
						console.log(tx);
						console.log(data);
					}
				,	that.onErrorSE
				);
			}
		,	"createTableSE":function() {
				this.runSE(
					myApp.SQL.createTable()
				,	[]
				);
			}
		,	"createItemSE" : function(todoItem) {
				var that = this;
				var addDate = new Date();
				addDate = addDate.toUTCString();
				this.runSE(
					myApp.SQL.createItem()
				,	[todoItem, addDate]
				,	function(tx, result) {
						var id = result.insertId;
						that.readItemSE(id, function(tx, result) {
							var item = new TodoItem(
								{
									"idDb" : id
								,	"todo" : result.rows.item(0).todo
								,	"date" : result.rows.item(0).added_on
								}
							);
							todoItemCollection.add(item);
							that.readItemsSE(that.readItemsC);
						});
					}
				,	that.onErrorSE
				);
			}
		,	"readItemsSE" : function(cb) {
				this.runSE(
					myApp.SQL.readItems()
				,	[]
				,	cb
				,	this.onErrorSE
				);
			}
		,	"readItemSE" : function(id, cb) {
				this.runSE(
					myApp.SQL.readItem()
				,	[id]
				,	cb
				,	this.onErrorSE
				);
			}
		,	"updateItemSE" : function(id, todo) {
				var that = this;
				this.runSE(
					myApp.SQL.updateItem()
				,	[todo, id]
				,	function(tx, result) {
						var item = todoItemCollection.where({"idDb" : id})[0];
						item.set({"todo" : todo});
						that.readItemsSE(that.readItemsC);
					}
				,	that.onErrorSE
				);
			}
		,	"deleteItemSE" :function(id) {
				var that = this;
				this.runSE(
					myApp.SQL.deleteItem()
				,	[id]
				,	function(tx, result) {
						var cid = todoItemCollection.where({"idDb" : id})[0].cid;
						todoItemCollection.remove(cid);
						that.readItemsSE(that.readItemsC);
					}
				,	that.onErrorSE
				);
			}

		,	"deleteAllItemsSE": function(id) {
				var that = this;
				this.runSE(
					myApp.SQL.deleteAllItems()
				,	[]
				,	function(tx, result) {
						todoItemCollection.reset();
						that.readItemsSE(that.readItemsC);
					}
				,	that.onErrorSE
				);
			}
		,	"onErrorSE" : function(tx, e) {
				alert("There has been an error: " + e.message);
			}
		,	"runSE" : function(sql, arr, success, failure) {
				myApp.DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	arr
					,	success
					,	failure
					);
				});
			}

		});

		var ChildCollection = ParentCollection.extend({
		  /*"createItemC" : function() {
			  ChildCollection.prototype.createItemC.call(this);
		  }*/
		});

		var todoItemCollection = new ChildCollection();
	// << BACKBONE COLLECTION

	/*todoItemCollection.createItemC();*/
	/*Did:
	1. TODOITEMCOLLECTION
	2. TodoItemCollection extends it
	3. todoItemCollection
	4. C and S into TODOITEMCOLLECTION
	5. var that = this
	6. TodoItemCollection.prototype.createItemC.call(this); és itt hiba!!!!*/










