
		/*
        Tutorial:

        First we create a model class, then a collection class.
        When we make an instance of the collection, it tries to create a table with the tablename and columns given in
        the model's default settings.

		First it runs the initialize method:
			- it calls openDB to create/open the websql database
			- then it calls createMasterTable, which:
				- creates a master table that holds data of 'normal' tables
				- reads this master table that allows the program to determine if our table to be created already exists or not
				- if it doesnt exist, it gets inserted into the master table and gets created too with method createTable
				- if it exists, the program determines if the table to be created has the same columns as
				the one already existing in master table with the same name
				- if it has the same columns, nothing happens
				- if it doesnt, an error message comes
        If everything is al right, a simple crud is created, with the possibilities of
        	- creating a row in the websql database (method create)
        	- reading a row (method read)
        	- updating a row (method update)
        	- deleting one row or every row (method delete, deleteAll)


        Addendum:
        	- getTableAttr fetches data from the model's defaults (tableName, columns)
        	- why do we need a master table? because websql doesnt make it possible to query the database so
        	we cant know its content - tables, columns etc. So we have to create a master table that acts like database.


        Example:
        We have a model with the following keys: name, age, job.
        We want a table with people as tablename
        we have a db name: peopledb

        We create all these, then insert a person with the following values: peter, 30, worker.
		Read and console log database.		// Object {id: 1, name: "peter", age: "30", job: "worker"}
        We then update its job to webdeveloper.
		Read and console log database.		// Object {id: 1, name: "peter", age: "30", job: "webdeveloper"}
        Then we create another person: monica, 22, model.
		Read and console log database. 		//	Object {id: 1, name: "peter", age: "30", job: "webdeveloper"}
		 										Object {id: 2, name: "monica", age: "22", job: "model"}
		Delete monica.
		Read and console log database.		// Object {id: 1, name: "peter", age: "30", job: "webdeveloper"}
		Delete all.
		Read and console log database. 		// nothing is shown


		Known issue:
		The instance is not ready when the program tries to go on, that is why we need set time out.


		Misc:
		When inserting, all columns should get a value, even if it is an empty sring
		When updating, the first array should contain a valid column name, or names, the second array should
		be exactly as long as the first one.

		*/

		var DB = null;

		var Model = Backbone.Model.extend({
			"defaults" : {
				"name"	: ""
			,	"age"	: ""
			,	"job"	: ""
			}
		});

		var ParentCollection = Backbone.Collection.extend({
			"initialize" : function(options) {
				this.options = options || {};

				this.openDB();
				this.createMasterTable();
			}
		,	"model" : Model
		,	"getTableAttr" : function() {
				var model 		= this.options.model
				,	tableName 	= this.options.tableName
				,	arrColumns 	= []
				;

				/*for(var key in model.attributes) {
					arrColumns.push(key);
				}*/

				var arrColumns = Object.keys(model.attributes);
			 // Model classt ha átadok, az csak egy fv és nem tudok konyerni belőle semmit sajnos
				return {
					"tableName"		: tableName
				,	"arrColumns"	: arrColumns
				};
			}
		,	"openDB" : function() {
				var dbName 	= "peopledb"
				,	dbSize 	= 5 * 1024 * 1024
				;

				DB = openDatabase(dbName, "1.0", dbName, dbSize
					,	function(data) {
						console.log("new database created: " + dbName);
							console.log(data);
						}
					);
			}
		,	"createMasterTable" : function() {
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				;
				var that = this;

				var sqlMC = "CREATE TABLE IF NOT EXISTS `master` (id INTEGER PRIMARY KEY ASC, table_name TEXT, columns BLOB);";
				var sqlMS = "SELECT * from `master`;";
				var sqlMI = "INSERT INTO `master` (table_name, columns) VALUES (?, ?);";

				DB.transaction(function(tx) {
					tx.executeSql(
						sqlMC
					,	[]
					,	function(tx, resultC) {
							tx.executeSql(
								sqlMS
							,	[]
							,	function(tx, resultS) {
									console.log("master tábla:");
									console.log(resultS.rows.length);
									var exists = false, arrAttr;
									for(var i = 0, ln = resultS.rows.length; i < ln; i++) {
										console.log(resultS.rows.item(i));
										if(resultS.rows.item(i).table_name === tableName) {
											exists = true;
											arrAttr = resultS.rows.item(i).columns.split(",");
										}
									}

									if(!exists) {
										console.log("a " + tableName + " tábla még nem létezik, mentem a masterba és létr is hozom");
										that.createTable();
										tx.executeSql(
											sqlMI
										,	[tableName, arrColumns]
										/*,	function() {
												that.createTable();
											}*/
										);
									} else {
										console.log("a tábla már létezik");
										if($(arrColumns).not(arrAttr).length == 0 && $(arrAttr).not(arrColumns).length == 0) {
											console.log("ugyanazok az oszlopok");
										} else {
											console.log("nem ugyanazok az oszlopok");
											console.log("eredeti");
											console.log(arrAttr);
											console.log("új");
											console.log(arrColumns);
										}
									}
								}
							)
						}
					);
				});
			}
		,	"createTable" : function() {

				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				;

				var sql = "CREATE TABLE IF NOT EXISTS "	+ tableName + " (id INTEGER PRIMARY KEY ASC, ";
				for(var i = 0, ln = arrColumns.length; i < ln; i++) {
					if(i < ln -1) {
						sql +=  arrColumns[i] + " TEXT, ";
					} else {
						sql +=  arrColumns[i] + " TEXT) ";
					}
				}
				console.log(sql);

				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	[]
					);
				});
			}
		,	"dropTable" : function(){
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				,	sql			=  'DROP ' + tableName
				;

				console.log(sql);
				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	[]
					);
				});
			}
		,	"create" : function(data) {
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				;

				var sql =  'INSERT INTO ' + tableName + '(';
				for(var i = 0, ln = arrColumns.length; i < ln; i++) {
					if(i < ln -1) {
						sql +=  arrColumns[i] + ", ";
					} else {
						sql +=  arrColumns[i] + ") VALUES (";
					}
				}
				for(var i = 0, ln = arrColumns.length; i < ln; i++) {
					if(i < ln -1) {
						sql += "?, ";
					} else {
						sql +=  "?)";
					}
				}
				//console.log(sql);

				var dataToBeInsterted = [];
				for(var i = 0, ln = data.length; i < ln; i++) {
					dataToBeInsterted.push(data[i]);
				}

				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	dataToBeInsterted
					,	function() {
							console.log("create siker")
						}
					,	function() {
							console.log("create kudarc")
						}
					);
				});
			}
		,	"read" : function() {
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				,	sql 		= "SELECT * FROM " + '`' + tableName + '`'
				;

				//console.log(sql);

				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	[]
					,	function(tx, result) {
							console.log("read siker");
							for(var i = 0, ln = result.rows.length; i < ln; i++) {
								console.log(result.rows.item(i));
							}
						}
					,	function() {
							console.log("read kudarc");
						}
					);
				});
			}
		,	"update" : function(id, arrColumnsToBeUpdated, arrValues) {
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				;

				var sql = "UPDATE " + tableName + " SET "
				for(var i = 0, ln = arrColumnsToBeUpdated.length; i < ln; i++) {
					if(i < ln - 1) {
						sql += arrColumnsToBeUpdated[i] + " = ?, ";
					} else {
						sql += arrColumnsToBeUpdated[i] + " = ? ";
					}

				}
				sql += "WHERE id = ?";

				arrValues.push(id);

				//console.log(sql);
				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	arrValues
					);
				});

			}
		,	"delete" : function(id) {
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				,	sql 		= "DELETE FROM " + tableName + " WHERE id = ?"
				;

				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	[id]
					);
				});

			}
		,	"deleteAll" : function() {
				var tableName	= this.getTableAttr().tableName
				,	arrColumns	= this.getTableAttr().arrColumns
				,	sql 		= "DELETE FROM " + tableName
				;

				DB.transaction(function(tx) {
					tx.executeSql(
						sql
					,	[]
					);
				});
			}
		});

		var todoCollection = new ParentCollection(
			{
				"tableName" 	: "people"
			,	"model" 		: new Model()
			}
		);


		setTimeout(function() {
			todoCollection.create(["peter", "30", "worker"]);
			todoCollection.read();
			todoCollection.update(1, ["job"], ["webdeveloper"]);
			todoCollection.read();
			todoCollection.create(["monica", "22", "model"]);
			todoCollection.read();
			todoCollection.delete(2);
			todoCollection.read();
			todoCollection.deleteAll();
			todoCollection.read();
		}, 100);









